/*
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any
 * remainder.
 *
 * What is the smallest positive number that is evenly divisible (divisible with no remainder) by all of the numbers form 1 to
 * 20?
 */

pub fn gcd(mut a: usize, mut b: usize) -> usize {
    while a != 0 && b != 0 {
        let c = a.max(b) - a.min(b);
        b = a.min(b);
        a = c;
    }
    a.max(b)
}

pub fn lcm(a: usize, b: usize) -> usize {
    if a == 0 && b == 0 {
        return 0;
    }
    (a*b)/gcd(a, b)
}

pub fn smallest_divisible_number_by_range<R: Iterator<Item = usize>>(range: R) -> usize {
    range.fold(1, |acc, x| lcm(acc, x))
}

#[test]
fn test_gcd() {
    assert_eq!(gcd(6,0), 6);
    assert_eq!(gcd(6,6), 6);
    assert_eq!(gcd(6,12), 6);
    assert_eq!(gcd(48, 18), 6);
}

#[test]
fn test_lcm() {
    assert_eq!(lcm(0, 0), 0);
    assert_eq!(lcm(21, 6), 42);
}

#[test]
fn test_smallest_divisible_number_by_range() {
    assert_eq!(smallest_divisible_number_by_range(1..=2), 2);
    assert_eq!(smallest_divisible_number_by_range(1..=3), 6);
    assert_eq!(smallest_divisible_number_by_range(1..=4), 12);
    assert_eq!(smallest_divisible_number_by_range(1..=5), 60);
    assert_eq!(smallest_divisible_number_by_range(1..=6), 60);
    assert_eq!(smallest_divisible_number_by_range(1..=7), 420);
    assert_eq!(smallest_divisible_number_by_range(1..=8), 840);
    assert_eq!(smallest_divisible_number_by_range(1..=9), 2520);
    assert_eq!(smallest_divisible_number_by_range(1..=10), 2520);
}

fn main() {
    const N: usize = 20;
    println!("The smallest positive number is evenly divisible by all of the numbers from 1 to {N} is {}", smallest_divisible_number_by_range(1..=N));
}
