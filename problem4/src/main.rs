/*
 * A palindromic number reads the same both ways. The largest palindrome made from the product of
 * two 2-digit numbers is 9009 = 91 × 99.
 *
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */

use std::ops::Range;

pub fn n_digit_number_min(n: usize) -> usize {
    match n {
        0 => std::process::abort(),
        1 => 0,
        2 => 10,
        n => n_digit_number_min(n-1) * 10,
    }
}

#[test]
fn test_digit_number_min() {
    assert_eq!(n_digit_number_min(1), 0);
    assert_eq!(n_digit_number_min(2), 10);
    assert_eq!(n_digit_number_min(3), 100);
    assert_eq!(n_digit_number_min(4), 1000);
}

pub fn n_digit_number_range(n: usize) -> Range<usize> {
    n_digit_number_min(n)..n_digit_number_min(n+1)
}

pub fn is_palindrome(n: usize) -> bool {
    let string = n.to_string();
    string.chars().eq(string.chars().rev())
}

#[test]
fn test_is_palindrome() {
    assert!(is_palindrome(1));
    assert!(is_palindrome(101));
    assert!(is_palindrome(1001));
    assert!(!is_palindrome(1011));
    assert!(!is_palindrome(102));
    assert!(is_palindrome(989));
    assert!(is_palindrome(999));
}

fn n_digit_largest_palindrome_product(n: usize) -> Option<usize> {
    let mut result = None;
    for i in n_digit_number_range(n).rev() {
        for j in n_digit_number_range(n).rev() {
            let product = i * j;
            match result {
                Some(k) if k > product => break,
                _ => {},
            }
            if is_palindrome(product)
            {
                result = Some(product);
            }
        }
    }
    result
}

#[test]
fn test_largest_palindrome_product() {
    assert_eq!(n_digit_largest_palindrome_product(2), Some(9009));
}

fn main() {
    const N: usize = 3;
    println!("The largest palinndrome made from the product of two {}-digit numbers is {:?}", N, n_digit_largest_palindrome_product(N));
}
