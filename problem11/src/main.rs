const NUMBERS: [[usize; 20]; 20] = [
    [08, 02, 22, 97, 38, 15, 00, 40, 00, 75, 04, 05, 07, 78, 52, 12, 50, 77, 91, 08],
    [49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48, 04, 56, 62, 00],
    [81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30, 03, 49, 13, 36, 65],
    [52, 70, 95, 23, 04, 60, 11, 42, 69, 24, 68, 56, 01, 32, 56, 71, 37, 02, 36, 91],
    [22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80],
    [24, 47, 32, 60, 99, 03, 45, 02, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50],
    [32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70],
    [67, 26, 20, 68, 02, 62, 12, 20, 95, 63, 94, 39, 63, 08, 40, 91, 66, 49, 94, 21],
    [24, 55, 58, 05, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72],
    [21, 36, 23, 09, 75, 00, 76, 44, 20, 45, 35, 14, 00, 61, 33, 97, 34, 31, 33, 95],
    [78, 17, 53, 28, 22, 75, 31, 67, 15, 94, 03, 80, 04, 62, 16, 14, 09, 53, 56, 92],
    [16, 39, 05, 42, 96, 35, 31, 47, 55, 58, 88, 24, 00, 17, 54, 24, 36, 29, 85, 57],
    [86, 56, 00, 48, 35, 71, 89, 07, 05, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58],
    [19, 80, 81, 68, 05, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77, 04, 89, 55, 40],
    [04, 52, 08, 83, 97, 35, 99, 16, 07, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66],
    [88, 36, 68, 87, 57, 62, 20, 72, 03, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69],
    [04, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18, 08, 46, 29, 32, 40, 62, 76, 36],
    [20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74, 04, 36, 16],
    [20, 73, 35, 29, 78, 31, 90, 01, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57, 05, 54],
    [01, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52, 01, 89, 19, 67, 48]];

enum Direction {
    East,
    SouthEast,
    South,
    SouthWest,
}

struct DirectionIterator {
    direction: Direction,
    x: isize,
    y: isize,
}

impl DirectionIterator {
    pub fn new(direction: Direction, x: usize, y: usize) -> DirectionIterator {
        DirectionIterator{
            direction: direction,
            x: x as isize,
            y: y as isize,
        }
    }
}

impl Iterator for DirectionIterator {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        if self.y < 0 || 20 <= self.y || self.x < 0 || 20 <= self.x {
            return None;
        }
        let result = NUMBERS[self.y as usize][self.x as usize];
        match self.direction {
            Direction::East => self.x += 1,
            Direction::SouthEast => { self.x += 1; self.y += 1; },
            Direction::South => self.y += 1,
            Direction::SouthWest => { self.x -= 1; self.y += 1; },
        }
        Some(result)
    }
}

#[test]
fn direction_iterator() {
    {
        let mut it = DirectionIterator::new(Direction::East, 0, 0);
        assert_eq!(it.next(), Some(8));
        assert_eq!(it.next(), Some(2));
        assert_eq!(it.next(), Some(22));
        assert_eq!(it.next(), Some(97));
    }
    {
        let mut it = DirectionIterator::new(Direction::South, 0, 0);
        assert_eq!(it.next(), Some(8));
        assert_eq!(it.next(), Some(49));
        assert_eq!(it.next(), Some(81));
        assert_eq!(it.next(), Some(52));
    }
    {
        let mut it = DirectionIterator::new(Direction::SouthEast, 0, 0);
        assert_eq!(it.next(), Some(8));
        assert_eq!(it.next(), Some(49));
        assert_eq!(it.next(), Some(31));
        assert_eq!(it.next(), Some(23));
    }
    {
        let mut it = DirectionIterator::new(Direction::SouthWest, 19, 0);
        assert_eq!(it.next(), Some(8));
        assert_eq!(it.next(), Some(62));
        assert_eq!(it.next(), Some(13));
        assert_eq!(it.next(), Some(37));
    }
    {
        let mut it = DirectionIterator::new(Direction::SouthEast, 8, 6);
        assert_eq!(it.next(), Some(26));
        assert_eq!(it.next(), Some(63));
        assert_eq!(it.next(), Some(78));
        assert_eq!(it.next(), Some(14));
        assert_eq!(DirectionIterator::new(Direction::SouthEast, 8, 6).take(4).product::<usize>(), 1788696);
    }
}

fn coordinates_matrix() -> impl Iterator<Item = (usize, usize)> {
    (0..20).flat_map(|i| (0..20).map(move |j| (i, j)))
}

#[test]
fn test_coordinates_matrix() {
    let mut it = coordinates_matrix();
    assert_eq!(it.next(), Some((0, 0)));
    assert_eq!(it.next(), Some((0, 1)));
    assert_eq!(it.next(), Some((0, 2)));
    let mut it = it.skip(17);
    assert_eq!(it.next(), Some((1, 0)));
    assert_eq!(it.next(), Some((1, 1)));
    assert_eq!(it.next(), Some((1, 2)));
}

fn product_matrix_iterator(n: usize) -> impl Iterator<Item = usize> {
    let east_it = coordinates_matrix().map(move |(x, y)| DirectionIterator::new(Direction::East, x, y).take(n).product());
    let south_east_it = coordinates_matrix().map(move |(x, y)| DirectionIterator::new(Direction::SouthEast, x, y).take(n).product());
    let south_it = coordinates_matrix().map(move |(x, y)| DirectionIterator::new(Direction::South, x, y).take(n).product());
    let south_west_it = coordinates_matrix().map(move |(x, y)| DirectionIterator::new(Direction::SouthWest, x, y).take(n).product());
    east_it.chain(south_east_it).chain(south_it).chain(south_west_it)
}

#[test]
fn test_product_matrix() {
    let mut it = product_matrix_iterator(4);
    assert_eq!(it.next(), Some(34_144usize));
    assert_eq!(it.next(), Some(9_507_960usize));
}

fn main() {
    const N: usize = 4;
    println!("The largest product of {N} adjacent numbers in the same direction (up, down, left, right or diagonally in the 20 × 20 grid is {} ", product_matrix_iterator(N).max().expect("There should be item to iterate over"));
}
