/*
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 *
 * Find the sum of all the primes below two million.
 */

use primes::Primes;

fn sum_of_primes(limit: usize) -> usize {
    Primes::new().take_while(|&prime| prime < limit).sum::<usize>()
}

#[test]
fn test_sum_of_primes() {
    const N: usize = 10;
    assert_eq!(sum_of_primes(N), 17);
}
fn main() {
    const N: usize = 2_000_000;
    println!("The sum of all the primes below {N} is {}", sum_of_primes(N));
}
