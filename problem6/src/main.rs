/*
 * The sum of the squares of the first ten natural numbers is:
 *      1² + 2² + … + 10² = 385
 *
 * The square of the sum of the first ten natural numbers is:
 *      (1 + 2 + … + 10)² = 55² = 3025.
 *
 * Hence the difference between the sum of the squares of the first ten natural numbers and the
 * square of the sum is 3025 - 385 = 2640.
 *
 * Find the difference between the sum of the squares of the first on hundred natural numbers and
 * the square of the sum.
 */

fn sum_of_squares<It: Iterator<Item = usize>>(it: It) -> usize {
    it.map(|n| n * n).sum()
}
fn square_of_sum<It: Iterator<Item = usize>>(it: It) -> usize {
    let sum: usize = it.sum();
    sum * sum
}

#[test]
fn test_diff() {
    let range = 1..=10;
    assert_eq!(square_of_sum(range.clone()) - sum_of_squares(range.clone()), 2640);
}

fn main() {
    let range = 1..=100;
    println!("The difference between the sum of the squares and the square of the sum of the first one hundred natural numbers is {}", square_of_sum(range.clone()) - sum_of_squares(range.clone()));
}
