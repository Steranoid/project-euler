/*
 *  A pythagorean triplet is a set of three natural numbers, a < b < c, for which, a²+b² = c².
 *
 *  For example 3² + 4² = 9 + 16 = 25 = 5².
 *  There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 *  Find the product abc.
 */

fn is_pythagorean_triplet(a: usize, b: usize, c: usize) -> bool {
    (a < b) && (b < c) && (a * a) + (b * b) == (c * c)
}

#[test]
fn is_pythagorean_triplet_test() {
    assert!(is_pythagorean_triplet(3, 4, 5));
}

/*  i  |  j  |  k  |  N  | N/3 | (N-i-1)/2 */
/*  1  |  2  |  7  |  10 | 3   | 4         */
/*  1  |  3  |  6  |  10 | 3   | 4         */
/*  1  |  4  |  5  |  10 | 3   | 4         */
/*  2  |  3  |  5  |  10 | 3   | 3         */
/*  1  |  2  |  8  |  11 | 3   | 4         */
/*  1  |  3  |  7  |  11 | 3   | 4         */
/*  1  |  4  |  6  |  11 | 3   | 4         */
/*  2  |  3  |  6  |  11 | 3   | 4         */
/*  2  |  4  |  5  |  11 | 3   | 4         */
/*  1  |  2  |  9  |  12 | 4   | 5         */
/*  1  |  3  |  8  |  12 | 4   | 5         */
/*  1  |  4  |  7  |  12 | 4   | 5         */
/*  1  |  5  |  6  |  12 | 4   | 5         */
/*  2  |  3  |  7  |  12 | 4   | 4         */
/*  2  |  4  |  6  |  12 | 4   | 4         */
/*  3  |  4  |  5  |  12 | 4   | 4         */
/*  i iterate from 1 to N / 3 not included */
/*  j iterate from i+1 to (N-i-1) / 2 included */
/*  k is fixed given an i and a j          */

fn main() {
    const N: usize = 1000;
    let range = (1..N/3)
        .flat_map(|i| (i+1..=(N-i-1)/2)
                  .map(move |j| (i, j, N - i - j)))
        .filter(|(i, j, k)| is_pythagorean_triplet(*i, *j, *k));
    for (i, j, k) in range {
        println!("Found a pythagorean triplet {i}, {j} and {k} which sum is {N}, the product of it is {}", i*j*k);
    }
}
