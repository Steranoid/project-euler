# project-euler

## Description
Project euler is an implementation in different langages of the project euler available [here](https://projecteuler.net), it’s only a sample to acknowledge JBD's programming skill.

## Authors and acknowledgment
This project is created and developped only by JBD aka Stera or Steranoid.

## License
This project is licensed over GPL3.
