/*
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
 * The sum of these multiples is 23.
 *
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */

fn f(n: usize) -> usize {
    (0usize..n).step_by(3).sum::<usize>() +
    (0usize..n).step_by(5).filter(|i| i % 3 != 0).sum::<usize>()
}

#[test]
fn test_sum_of_multiples_of_3_or_5() {
    const N: usize = 10;
    assert_eq!(f(N), 23);
}

fn main() {
    const N: usize = 1000;
    println!("The sum of all the multiples of 3 or 5 below {} is {}", N, f(N));
}
