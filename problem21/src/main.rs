fn proper_divisors(n: usize) -> impl Iterator<Item = usize> {
    (1..=n/2).filter(move |number| n % number == 0)
}

#[test]
fn test_proper_divisors() {
    {
        let mut it = proper_divisors(220);
        assert_eq!(it.next(), Some(1));
        assert_eq!(it.next(), Some(2));
        assert_eq!(it.next(), Some(4));
        assert_eq!(it.next(), Some(5));
        assert_eq!(it.next(), Some(10));
        assert_eq!(it.next(), Some(11));
        assert_eq!(it.next(), Some(20));
        assert_eq!(it.next(), Some(22));
        assert_eq!(it.next(), Some(44));
        assert_eq!(it.next(), Some(55));
        assert_eq!(it.next(), Some(110));
        assert_eq!(it.next(), None);
    }
    {
        let mut it = proper_divisors(284);
        assert_eq!(it.next(), Some(1));
        assert_eq!(it.next(), Some(2));
        assert_eq!(it.next(), Some(4));
        assert_eq!(it.next(), Some(71));
        assert_eq!(it.next(), Some(142));
        assert_eq!(it.next(), None);
    }
}

fn d(n: usize) -> usize {
    static CACHE: std::sync::RwLock<std::collections::BTreeMap<usize, usize>> = std::sync::RwLock::new(std::collections::BTreeMap::new());
    if let Some(result) = CACHE.read().expect("Should be able to read mutex").get(&n) {
        return *result;
    }
    let sum = proper_divisors(n).sum();
    let _ = CACHE.write().expect("Should be able to write the mutex").insert(n, sum);
    sum
}

#[test]
fn test_d() {
    assert_eq!(d(220), 284);
    assert_eq!(d(284), 220);
}

fn is_amicable_number(i: usize) -> Result<usize, usize> {
    let j = d(i);
    if i == j {
        return Err(j);
    }
    match d(j) {
        n if n == i => Ok(j),
        _ => Err(j),
    }
}

#[test]
fn test_amicable_numbers() {
    assert_eq!(is_amicable_number(6), Err(6));
    assert_eq!(is_amicable_number(220), Ok(284));
    assert_eq!(is_amicable_number(221), Err(31));
}

fn range_of_amicable_numbers(n: usize) -> impl Iterator<Item = usize> {
    (1..=n).filter(|&number| is_amicable_number(number).is_ok())
}

#[test]
fn test_range_of_amicable_numbers() {
    let mut it = range_of_amicable_numbers(300);
    assert_eq!(it.next(), Some(220));
    assert_eq!(it.next(), Some(284));
    assert_eq!(it.next(), None);
}

fn main() {
    const N: usize = 10_000;
    println!("The sum of all the amicable numbers under {N} is {}", range_of_amicable_numbers(N).sum::<usize>());
}
