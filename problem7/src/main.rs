/*
 * By listing the first six prime numbers: 2, 3, 5, 7, 11 and 13, we can see that the 6th prime is
 * 13.
 *
 * What is the 10 001st prime number?
 */

use primes::Primes;

#[test]
fn sixth_prime() {
    const N: usize = 6;
    let mut primes = Primes::new();
    assert_eq!(primes.nth(N-1), Some(13));
}

fn main() {
    const N: usize = 10_001;
    let mut primes = Primes::new();
    println!("The {N}th prime number is {}", primes.nth(N-1).unwrap());
}
