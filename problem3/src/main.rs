/*
 * The prime factors of 13195 are 5, 7, 13 and 29.
 *
 * What is the largest prime factor of the number 600 851 475 143?
 */

use primes::Primes;
use primes::PrimeNode;

#[test]
pub fn test_prime_factors() {
    let mut primes = Primes::new();
    assert_eq!(primes.decompose(0).last(), None);
    assert_eq!(primes.decompose(1).last(), None);
    assert_eq!(primes.decompose(2).last(), Some(&PrimeNode{prime: 2, exposant: 1}));
    assert_eq!(primes.decompose(3).last(), Some(&PrimeNode{prime: 3, exposant: 1}));
    assert_eq!(primes.decompose(4).last(), Some(&PrimeNode{prime: 2, exposant: 2}));
    assert_eq!(primes.decompose(5).last(), Some(&PrimeNode{prime: 5, exposant: 1}));
    assert_eq!(primes.decompose(13_195).last(), Some(&PrimeNode{prime: 29, exposant: 1}));
}

pub fn main() {
    const N: usize = 600_851_475_143;
    let mut primes = Primes::new();
    let prime_factors = primes.decompose(N);
    match prime_factors.last() {
        None => println!("{} has no prime factors", N),
        Some(&PrimeNode{ prime, .. }) => {
            println!("The prime factors of {} are {:?}", N, prime_factors);
            println!("The largest prime factors of {} is {}", N, prime);
        }
    }
}
