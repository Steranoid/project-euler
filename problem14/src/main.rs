/*
 * The following iterative sequence is defined for the set of positive integers:
 *
 *      n -> n/2 (n is even)
 *      n -> 3n + 1 (n is odd)
 *
 * Using the rule above and starting with 13, we generate the following sequence:
 *      13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1.
 *
 * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
 * Although it has no been proved yet (Collatz Problem), it is thought that all starting numbers
 * finish at 1.
 *
 * Which starting number, under one million, produces the longest chain?
 *
 * NOTE: Once the chain starts the terms are allowed to go above one million
 */

struct CollatzSequence {
    n: usize,
}

impl CollatzSequence {
    pub fn new(n: usize) -> CollatzSequence {
        CollatzSequence {
            n: n,
        }
    }
}

impl Iterator for CollatzSequence {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        let result = self.n;
        match self.n % 2 {
            0 => self.n = self.n / 2,
            _ => self.n = 3 * self.n + 1,
        }
        Some(result)
    }
}

#[test]
fn collatz_sequence() {
    let mut cs = CollatzSequence::new(13);
    assert_eq!(cs.next(), Some(13));
    assert_eq!(cs.next(), Some(40));
    assert_eq!(cs.next(), Some(20));
    assert_eq!(cs.next(), Some(10));
    assert_eq!(cs.next(), Some(5));
    assert_eq!(cs.next(), Some(16));
    assert_eq!(cs.next(), Some(8));
    assert_eq!(cs.next(), Some(4));
    assert_eq!(cs.next(), Some(2));
    assert_eq!(cs.next(), Some(1));
}

type CollatzSizes = std::collections::BTreeMap<usize, usize>;

fn cached_collatz_sequence_length(sizes: &mut CollatzSizes, mut seq: CollatzSequence) -> usize {
    let number = seq.next().unwrap();
    if number == 1 {
        return 1;
    }
    match sizes.get(&number).cloned() {
        Some(i) => i,
        None => {
            let size = cached_collatz_sequence_length(sizes, seq) + 1;
            sizes.insert(number, size);
            size
        },
    }
}

#[test]
fn cached_collatz_sequence_length_test() {
    const N: usize = 13;
    let mut sizes = CollatzSizes::new();
    assert_eq!(cached_collatz_sequence_length(&mut sizes, CollatzSequence::new(N)), 10);
    let mut it = sizes.into_iter();
    assert_eq!(it.next(), Some((2, 2)));
    assert_eq!(it.next(), Some((4, 3)));
    assert_eq!(it.next(), Some((5, 6)));
    assert_eq!(it.next(), Some((8, 4)));
    assert_eq!(it.next(), Some((10, 7)));
    assert_eq!(it.next(), Some((13, 10)));
    assert_eq!(it.next(), Some((16, 5)));
    assert_eq!(it.next(), Some((20, 8)));
    assert_eq!(it.next(), Some((40, 9)));
}

fn main() {
    const N: usize = 1_000_000;
    let mut sizes = CollatzSizes::new();
    println!("The starting number under {N} that produces the longest collatz sequence is {}", (1..N).map(|number| (number, cached_collatz_sequence_length(&mut sizes, CollatzSequence::new(number)))).fold((1, 1), |(n, size), (n2, size2)| { if size < size2 { return (n2, size2); } (n, size) }).0);
}
