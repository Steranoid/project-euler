#[derive(Clone)]
pub struct Primes {
    primes: std::vec::Vec<usize>,
    n: usize,
}

#[derive(Debug, PartialEq, Eq)]
pub struct PrimeNode {
    pub prime: usize,
    pub exposant: usize,
}

impl Primes {
    pub fn new() -> Primes {
        Primes{
            primes: std::vec::Vec::<usize>::new(),
            n: 0,
        }
    }
    fn rewind(&mut self) -> &mut Self {
        self.n = 0;
        self
    }

    pub fn is_prime(&mut self, candidate: usize) -> bool {
        candidate > 1 && self.rewind()
            .take_while(|prime| prime * prime <= candidate)
            .all(|prime| candidate % prime != 0)
    }

    pub fn decompose(&mut self, mut number: usize) -> std::vec::Vec<PrimeNode> {
        let mut result = std::vec::Vec::new();
        for prime in self.rewind() {
            if number < prime {
                break;
            }
            if number % prime == 0 {
                let mut decomposition = PrimeNode{prime: prime, exposant: 0};
                while number % prime == 0 {
                    number /= prime;
                    decomposition.exposant += 1;
                }
                result.push(decomposition);
            }
        }
        result
    }
}

impl Iterator for Primes {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        while self.primes.len() <= self.n {
            match self.primes.last() {
                None => self.primes.push(2),
                Some(2) => self.primes.push(3),
                Some(i) => {
                    let mut candidate = i + 2;

                    while !self.primes.iter()
                            .take_while(|&prime| *prime * *prime <= candidate)
                            .all(|prime| candidate % prime != 0) {
                        candidate += 2;
                    }

                    self.primes.push(candidate);
                }
            }
        }
        let result = self.primes.get(self.n).cloned();
        self.n += 1;
        result
    }
}

#[test]
fn test_primes() {
    let mut primes = Primes::new();
    assert_eq!(primes.next(), Some(2));
    assert_eq!(primes.next(), Some(3));
    assert_eq!(primes.next(), Some(5));
    assert_eq!(primes.next(), Some(7));
    assert_eq!(primes.next(), Some(11));
    assert_eq!(primes.next(), Some(13));
    assert_eq!(primes.next(), Some(17));
    assert_eq!(primes.next(), Some(19));
    assert_eq!(primes.next(), Some(23));
    assert_eq!(primes.next(), Some(29));
    assert_eq!(primes.next(), Some(31));
}

#[test]
fn test_is_prime() {
    let mut primes = Primes::new();
    assert!(!primes.is_prime(0));
    assert!(!primes.is_prime(1));
    assert!( primes.is_prime(2));
    assert!( primes.is_prime(3));
    assert!(!primes.is_prime(4));
    assert!( primes.is_prime(5));
    assert!(!primes.is_prime(6));
    assert!( primes.is_prime(7));
    assert!(!primes.is_prime(8));
    assert!(!primes.is_prime(9));
}

#[test]
fn test_decompose() {
    let mut primes = Primes::new();
    assert_eq!(primes.decompose(0),  std::vec::Vec::new());
    assert_eq!(primes.decompose(1),  std::vec::Vec::new());
    assert_eq!(primes.decompose(2),  vec!(PrimeNode{ prime: 2, exposant: 1 },));
}
