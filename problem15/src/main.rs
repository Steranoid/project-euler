/*
 * Starting in the top left corner of a 2 × 2 grid, and only being able to move to the right and down, there are exactly routes to the bottom right corner.
 *
 *      ·—·—·   ·—· ·   ·—· ·
 *          |     |       |
 *      · · ·   · ·—·   · · ·
 *          |       |     |
 *      · · ·   · · ·   · ·—·
 *
 *      · · ·   · · ·   · · ·
 *      |       |       |
 *      ·—·—·   ·—· ·   · · ·
 *          |     |     |
 *      · · ·   · ·—·   ·—·—·
 *
 * How many such routes are there through a 20 × 20 grid?
 *
 *      ·—·   · ·
 *        |   |
 *      · ·   ·—·
 *
 * There are 2 routes for 1 × 1 grid
 */

type LatticePathsCache = std::collections::BTreeMap<(usize, usize), usize>;

fn lattice_paths(cache: &mut LatticePathsCache, width: usize, length: usize) -> usize {
    println!("Calling lattice_paths({width}, {length})");
    if width == 0 || length == 0 {
        return 1;
    }
    match cache.get(&(width, length)).cloned() {
        Some(paths) => paths,
        None => {
            let result = lattice_paths(cache, width-1, length) + lattice_paths(cache, width, length-1);
            cache.insert((width, length), result);
            result
        }
    }
}

#[test]
fn test_lattice_paths() {
    let mut paths = LatticePathsCache::new();
    assert_eq!(lattice_paths(&mut paths, 1, 1), 2);
    assert_eq!(lattice_paths(&mut paths, 2, 2), 6);
    assert_eq!(lattice_paths(&mut paths, 1, 0), 1);
    assert_eq!(lattice_paths(&mut paths, 2, 0), 1);
    assert_eq!(lattice_paths(&mut paths, 5, 0), 1);
    assert_eq!(lattice_paths(&mut paths, 0, 1), 1);
    assert_eq!(lattice_paths(&mut paths, 0, 2), 1);
    assert_eq!(lattice_paths(&mut paths, 0, 5), 1);
}

fn main() {
    const N: usize = 20;
    let mut paths = LatticePathsCache::new();
    println!("There are {} routes through a {N} × {N} grid", lattice_paths(&mut paths, N, N));
}
